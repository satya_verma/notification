module.exports = function (mongo, ObjectID, url, assert, dbb) {
    var notification_module = {


        //add blog
        add_notification: function (new_notification, callBack) {
            try {
                mongo.connect(url, {
                    useNewUrlParser: true,
                    useUnifiedTopology: true
                }, function (err, db) {
                    assert.equal(null, err);
                    db.db().collection(dbb.NOTIFICATIONS).insertOne(new_notification, function (err, result) {
                        if (err) {
                            callBack(null, true, "Error Ocurred");
                        } else {
                            callBack(result, false, "Notification Added Successfully");
                        }
                        db.close();
                    });
                });

            } catch (e) {
                callBack(null, true, e)
            }
        },

    }
    return notification_module;
}